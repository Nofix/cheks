//https://docs.rs/dialog/0.1.1/dialog/struct.Input.html
use conrod_core::Borderable;
use conrod_core::Sizeable;
use conrod_core::Widget;

use gamelogic::game::player;
use gamelogic::game::player::Board;
use gamelogic::game::player::TeamColor;
use gamelogic::game::player::Coord;
use gamelogic::game::piece_logic::Piece;
use gamelogic::game::piece_logic::PieceKind;
use gamelogic::game::player::GameState;

use crate::client_messenger::*;
use gamelogic::messenger::messenger::MessageType;

use piston::input::mouse::MouseCursorEvent;
use piston::Button::Mouse;
use crate::piston::Window;
use piston::window::WindowSettings;
use piston::*;

use glutin_window::GlutinWindow;

use opengl_graphics::{GlGraphics, OpenGL, Texture,TextureSettings};
use opengl_graphics::GlyphCache;

use graphics::{Image, clear};
use graphics::Transformed;
use graphics::rectangle;
use graphics::text::Text;
use graphics::text;
use graphics::rectangle::square;
use graphics::rectangle::rectangle_by_corners;

use std::net::TcpStream;
use std::path::PathBuf;

use std::sync::mpsc::channel;
use std::sync::mpsc::Sender;
use std::sync::mpsc::Receiver;
use std::sync::atomic::{AtomicBool,Ordering};
use std::sync::Arc;

use std::thread;
const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
const WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];

const CASE_SIZE: u64 =  56;
const WIDTH_OFFSET: f64 = 140.0;
// static BROWN: [f32; 4] = piston_window::color::hex("0xe3c9a4");


pub fn ressource_path(res_path: &str) -> PathBuf {
    [env!("CARGO_MANIFEST_DIR"), "assets/", res_path].iter().collect()
}

pub fn load_texture(res_path: &str) -> Texture {
    let path: PathBuf = ressource_path(res_path);
    return opengl_graphics::Texture::from_path(path, &TextureSettings::new()).unwrap();
    // Path::new(concat!(env!("CARGO_MANIFEST_DIR"), "/assets/pieces/Bguard.png.png"))
}



pub struct BoardGUI {
    board: Board,
    board_colors: [[f32; 4]; 4],
    pieces_images: [PieceRessource;5],
}

impl BoardGUI {
    pub fn new(board: Board) -> BoardGUI {
        BoardGUI{ 
            board: board, 
            board_colors: [
                    // [0.48, 0.93, 1.0, 1.0],
                    piston_window::color::hex("FFF8DC"), // light brown FFF8DC
                    // piston_window::color::WHITE, 
                    // [0.89019614,0.78823537,0.64313731,1.0],  // brown 0xE3C9A4
                    piston_window::color::hex("DEB887"), // brown DEB887
                    piston_window::color::hex("CC0000"),            // red         0xCC0000

                    // lightblue.
                    piston_window::color::hex("ADD8E6")            
                ],
            pieces_images: [
                PieceRessource{
                    image: graphics::Image::new().rect(square(0.0, 0.0, 46.0)),
                    texture_by_color: [ // bishop
                        load_texture("pieces/Wbishop.png"),
                        load_texture("pieces/Bbishop.png"),
                    ]
                },
                PieceRessource{
                    image: graphics::Image::new().rect(square(0.0, 0.0, 46.0)),
                    texture_by_color: [ // rook
                        load_texture("pieces/Wtower.png"),
                        load_texture("pieces/Btower.png"),
                    ]
                },
                PieceRessource{
                    image: graphics::Image::new().rect(square(0.0, 0.0, 46.0)),
                    texture_by_color: [ // guard
                        load_texture("pieces/Wrook.png"),
                        load_texture("pieces/Brook.png"),
                    ]
                },
                PieceRessource{
                    image: graphics::Image::new().rect(square(0.0, 0.0, 46.0)),
                    texture_by_color: [ // bishop
                        load_texture("pieces/Wguard.png"),
                        load_texture("pieces/Bguard.png"),
                    ]
                },
                PieceRessource{
                    image: graphics::Image::new().rect(square(0.0, 0.0, 46.0)),
                    texture_by_color: [ // mountain
                        load_texture("pieces/mountain.png"),
                        load_texture("pieces/mountain.png"),
                    ]
                }
            ],
        }
    }
}


pub struct PieceRessource {
    image: Image,
    texture_by_color: [Texture; 2]
}
enum PieceColor{
    White = 0,
    Black = 1
}
enum board_colors {
    White = 0,
    Brown = 1,
    Red = 2,
    LightBlue = 3,
}
enum Pieces_images {
    Bguard = 0
}

pub struct App <'a> {
    gl: GlGraphics, // OpenGL drawing backend.
    board_gui: BoardGUI,
    playing_color: TeamColor,
    mouse_pos: [f64;2],
    selected_piece: Option<Coord>,
    teamcolor: TeamColor,
    game_state: GameState,
    mountain_count: (u8,u8),
    fd: TcpStream,
    waiting_for_server: Arc<AtomicBool>,
    sender: Sender<(Option<Coord>, Option<Coord>)>, 
    receiver: Receiver<(Option<Coord>, Option<Coord>)>,
    glyph_cache: GlyphCache<'a>,
    winner: Option<TeamColor>
    // window: GlutinWindow
}

impl<'a> App<'a> {

    fn new(gl: GlGraphics, 
        board_gui: BoardGUI, 
        playing_color: TeamColor, 
        mouse_pos: [f64;2], 
        selected_piece: Option<Coord>, 
        teamcolor: TeamColor, 
        game_state: GameState, 
        mountain_count: u8, 
        fd: TcpStream, 
        waiting_for_server: Arc<AtomicBool>, 
        sender: Sender<(Option<Coord>, Option<Coord>)>, 
        receiver: Receiver<(Option<Coord>, Option<Coord>)>,
        ) -> App<'a>{
        
        App {
            gl: gl, // OpenGL drawing backend.
            board_gui:board_gui,
            playing_color:playing_color,
            mouse_pos:mouse_pos,
            selected_piece:selected_piece,
            teamcolor:teamcolor,
            game_state:game_state,
            mountain_count:(mountain_count,mountain_count),
            fd:fd,
            waiting_for_server: waiting_for_server,
            sender: sender,
            receiver: receiver,
            glyph_cache: GlyphCache::new(
                ressource_path("OpenSans-Regular.ttf"),
                    (),
                    TextureSettings::new()
                ).expect("Unable to load font"),
            winner: None
        }
    }

    fn render(&mut self, args: &RenderArgs) {
        let square = rectangle_by_corners(0.0, 0.0, 1.0, 1.0);
        let board = &self.board_gui;
        let teamcolor = self.teamcolor;
        let glyphs = &mut self.glyph_cache;
        let mut game_state_txt = format!("{}", self.game_state);
        if self.game_state == GameState::EndGame {
            game_state_txt.push_str(format!(" {} won !", self.winner.unwrap()).as_str());
        }
        let playing_color_txt = format!("{} playing", self.playing_color);

        self.gl.draw(args.viewport(), |c, gl| {
            clear(BLACK, gl);



            // Draw board
            for (i, row) in board.board.board.iter().enumerate() {
                for (j, case) in row.iter().enumerate() {
                    let gl_case_color : [f32; 4];
                    if case.selected {
                        gl_case_color = board.board_colors[board_colors::LightBlue as usize];
                    } else {
                        gl_case_color = match case.get_color() {
                            player::CaseColor::White      => board.board_colors[board_colors::White as usize],
                            player::CaseColor::Black      => board.board_colors[board_colors::Brown as usize],
                            player::CaseColor::Red        => board.board_colors[board_colors::Red as usize],
                            // player::CaseColor::Selected   => {
                                // println!("Selecting lightblue");
                                // board.board_colors[board_colors::LightBlue as usize]
                            // }
                            player::CaseColor::OutOfBound => piston_window::color::TRANSPARENT,
                        };
                    }
                    
                    
                    let coord = App::get_screen_pos_from_teamcolor(teamcolor, Coord{x:i,y:j});
                    let x = 46.0 * coord.x as f64;
                    let y = 46.0 * coord.y as f64;
                    let transformation = c
                                     .transform.trans(x,y)
                                     .scale(46.0, 46.0);
                    rectangle(gl_case_color, square, transformation, gl);

                    // Draw pieces with their texture
                    if !case.piece.is_none() {
                        let p_color: usize = match case.piece.unwrap().color {
                            TeamColor::White => board_colors::White as usize,
                            _ => 1
                        };
                        let p_index = case.piece.unwrap().kind as usize;
                        board.pieces_images[p_index].image.draw(
                            &board.pieces_images[p_index].texture_by_color[p_color],
                            &c.draw_state,
                            c.transform.trans(x,y), 
                            gl
                        )
                    }
                }
            }

            // draw seperation line
            let transformation_line = c
                                     .transform.trans(0.0,319.0)
                                     .scale(414.0, 6.0);
            rectangle(board.board_colors[board_colors::Red as usize], square, transformation_line, gl);
            
            text(piston_window::color::WHITE,
                20,
                game_state_txt.as_str(),
                glyphs, 
                c.transform.trans(10.0, 46.0/2.0).zoom(0.5),
                gl).unwrap();
            text(piston_window::color::WHITE,
                20,
                playing_color_txt.as_str(),
                glyphs, 
                c.transform.trans(212.0 + 75.0, 46.0/2.0).zoom(0.5),
                gl).unwrap();
        });
    }

    pub fn get_screen_pos_from_teamcolor(teamcolor: TeamColor, c: Coord) -> Coord{
        if teamcolor == TeamColor::White {
            return Coord{x: Board::W - c.x - 1, y: Board::H - c.y - 1};
        }
        c
    }

    fn update(&mut self, _args: &UpdateArgs) {
    }

    fn key_press(&mut self, args: &Button, window: &mut Window) {
        if *args == Mouse(MouseButton::Left) {
            // [414, 644]
            let cell_size = window.draw_size().width / 9.0;
            let (x, y): (usize, usize) = ((self.mouse_pos[0] / cell_size) as usize, (self.mouse_pos[1] / cell_size) as usize);
            let c = App::get_screen_pos_from_teamcolor(self.teamcolor, Coord{x,y});
            let x = c.x;
            let y = c.y;
            self.handle_cell_clic(x,y);
        } else if *args == Mouse(MouseButton::Right) {
            self.board_gui.board.reset_selections();
            self.selected_piece = None;
        }
    }

    // fn recv_information(&mut self) -> Coord {
    //     println!("[client] Receiving info into function");
    //     return match self.game_state {
    //         GameState::SetupMountain => {
    //             let c = recv_mountain_coord(&mut self.fd.try_clone().unwrap());
    //             println!("[client] Received info from server");
    //             self.board_gui.board.board[c.x][c.y].piece = Some(Piece::new(!self.teamcolor, c, PieceKind::Mountain));
    //             c
    //         },
    //         GameState::Playing => todo!(),
    //         GameState::EndGame => todo!(),
    //     };
    // }

    fn handle_cell_clic(&mut self, x: usize, y: usize) {
        if self.playing_color != self.teamcolor {
            return;
        }

        match self.game_state {
            GameState::SetupMountain => self.handle_mountain_placement(x,y),
            GameState::Playing => self.handle_piece_movement(x,y),
            GameState::EndGame => self.handle_engame(),
        };

        // println!("{}",self.boardGUI.board);
    }
    fn handle_engame(&mut self){

    }
    
    fn handle_mountain_placement(&mut self, x:usize, y:usize) {
        if self.board_gui.board.board[x][y].piece.is_none() 
            && Board::is_in_team_bound(Coord{x,y}, self.teamcolor)
            && self.board_gui.board.can_set_mountain(Coord{x,y}){ 
            // && Board::get_number_mountain_on_row(y) < 3 {
            if send_mountain_coord(&mut self.get_stream(), Coord{x,y}) {
                self.board_gui.board.board[x][y].piece = Some(
                    Piece::new(self.teamcolor, Coord::new(x, y), PieceKind::Mountain)
                );
                self.mountain_count.0 -= 1;
                if self.mountain_count.0 == 0 
                    && self.mountain_count.1 == 0 {
                    self.game_state = GameState::Playing;
                }
                
                self.change_turn();
            } 
        }
    }

    fn check_win(&mut self){
        // Black wins
        if (self.board_gui.board.board[1][4].piece.is_none() || self.board_gui.board.board[1][4].piece.unwrap().kind != PieceKind::Tower) &&
           (self.board_gui.board.board[7][4].piece.is_none() || self.board_gui.board.board[7][4].piece.unwrap().kind != PieceKind::Tower) {
            if self.board_gui.board.board[4][0].piece.is_some() && self.board_gui.board.board[4][0].piece.unwrap().color == TeamColor::Black {
                self.game_state = GameState::EndGame;
                self.winner = Some(TeamColor::Black);
            }
        }
        // White wins
        if (self.board_gui.board.board[1][9].piece.is_none() || self.board_gui.board.board[1][9].piece.unwrap().kind != PieceKind::Tower) &&
           (self.board_gui.board.board[7][9].piece.is_none() || self.board_gui.board.board[7][9].piece.unwrap().kind != PieceKind::Tower) {
            if self.board_gui.board.board[4][13].piece.is_some() && self.board_gui.board.board[4][13].piece.unwrap().color == TeamColor::White {
                self.game_state = GameState::EndGame;
                self.winner = Some(TeamColor::White);
            }
        }
    }

    fn handle_piece_movement(&mut self, x:usize, y:usize) {
        self.board_gui.board.reset_selections();
        if self.selected_piece.is_some()  // if a piece was selected
            && !(self.board_gui.board.board[x][y].piece.is_some()  // and that we do not clic on a piece of our color
                    && self.board_gui.board.board[x][y].piece.unwrap().color == self.teamcolor) {
                let coord = self.selected_piece.unwrap();
                self.selected_piece = None;
                if self.board_gui.board.board[coord.x][coord.y].piece.unwrap().move_to(&mut self.board_gui.board, Coord{x,y}) {
                    send_piece_move(&mut self.fd.try_clone().unwrap(), coord, Coord{x,y});
                    // enemy turn to play
                    self.check_win();
                    self.change_turn();
                    return;
            }
            
        } 

        if self.board_gui.board.board[x][y].piece.is_some() {
            if self.board_gui.board.board[x][y].piece.unwrap().color == self.teamcolor {
                self.selected_piece = Some(Coord{x,y});
                let possible_moves = self.board_gui.board.board[x][y].piece.unwrap().get_possible_moves(&self.board_gui.board);
                for p_move in possible_moves {
                    // println!("Can move {} {}", p_move.x, p_move.y);
                    self.board_gui.board.board[p_move.x][p_move.y].selected = true;
                }
            }
        }
        
    }
    fn get_stream(&mut self) -> TcpStream {
        self.fd.try_clone().unwrap()
    }

    fn change_turn(&mut self) {
        self.playing_color = !self.playing_color;
        self.waiting_for_server.store(true, Ordering::Relaxed);
        self.sender.send((None, None));
    }

}

fn recv_enemy_moves(mut stream: &mut TcpStream, sender: Sender<(Option<Coord>, Option<Coord>)>, receiver: Receiver<(Option<Coord>, Option<Coord>)>, b: Arc<AtomicBool>, playing_first: bool) {
    if playing_first {
        receiver.recv(); // wait for main thread notification
    }
    let mut cnt = 6;
    while cnt != 0 {
        println!("[client] Receiving info into function");
        let c = recv_mountain_coord(&mut stream);
        println!("  [client] Received info from server");
        sender.send((Some(c), None)).unwrap();
        cnt = cnt - 1;    
        receiver.recv(); // wait for main thread notification
        println!("cnt is now {}", cnt);
    }
    println!("Now receiving pieces moves");
    // if playing_first {
        // receiver.recv(); // wait for main thread notification
    // }
    loop {
        println!("[client] Receiving info into function");
        let (o, d) = recv_piece_move(&mut stream);
        println!("  [client] Received move from enemy");
        sender.send((Some(o), Some(d))).unwrap();
        receiver.recv();  // wait for main thread notification
    }
}


// use glutin_window::UserEvent
fn init_gui() -> (opengl_graphics::GlGraphics, GlutinWindow){
    let window:GlutinWindow = WindowSettings::new(
        "Cheks",
        (414, 644)
    )   
    // .graphics_api(opengl)
    // .opengl(opengl_graphics::OpenGL::V4_5)
    .exit_on_esc(true)
    .resizable(false)
    .build()
    .unwrap();
    let gl = GlGraphics::new(OpenGL::V4_5);
    (gl, window)
}

pub fn gui(teamcolor: TeamColor, stream: TcpStream)  {
    let (gl, mut window) = init_gui();

    let waiting_for_server = match teamcolor { 
        TeamColor::White => Arc::new(AtomicBool::new(false)),
        TeamColor::Black => Arc::new(AtomicBool::new(true)) 
    };
    widget_ids! {
        struct Ids {
            sometextbox,
        }
    }
    let mut ui = conrod_core::UiBuilder::new([320.0 as f64, 320.0 as f64]).build();
    let mut ids = Ids::new(ui.widget_id_generator());
    let mut ui = ui.set_widgets();

            // A text box in which we can mutate a single line of text, and trigger reactions via the
        // `Enter`/`Return` key.
        for event in conrod_core::widget::TextBox::new("Hello")
            .font_size(20)
            .w_h(320.0, 40.0)
            .border(320.0)
            .set(ids.sometextbox, &mut ui)
            // .border_color(app.bg_color.invert().plain_contrast())
            // .color(app.bg_color.invert())
        {
            match event {
                conrod_core::widget::text_box::Event::Enter          => println!("TextBox"),
                conrod_core::widget::text_box::Event::Update(string) => println!("TextBox"),
            }
        }


    // Server will always be giving Coord for mountains coord,
    // and a couple of coords when pieces moves
    let (sender_from_thread, receiver_of_thread) = channel::<(Option<Coord>, Option<Coord>)>();
    let (sender_of_thread, receiver_from_thread) = channel::<(Option<Coord>, Option<Coord>)>();
    
    let mut app = App::new(
        gl,
        BoardGUI::new(Board::new()),
        TeamColor::White,
        [0.0;2],
        None,
        teamcolor,
        GameState::SetupMountain,
        6,
        stream,
        Arc::clone(&waiting_for_server),
        sender_of_thread,
        receiver_of_thread,
        
    );
    
    
    let mut s = app.fd.try_clone().unwrap();

    let b = if app.teamcolor == TeamColor::White {true} else {false};

    thread::spawn(move || {
        recv_enemy_moves(&mut s, sender_from_thread, receiver_from_thread, Arc::clone(&waiting_for_server), b);
    });


    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut window) {
        
        if let Some(args) = e.render_args() {
            app.render(&args);
        }

        if let Some(args) = e.update_args() {
            app.update(&args);
        }
        if let Some(ref args) = e.press_args() {
            app.key_press(&args, &mut window);
        }
        e.mouse_cursor(|pos| {
           app.mouse_pos = pos;
        });

        match app.receiver.try_recv() {
            Ok((c, c2)) => {
                match app.game_state {
                    GameState::SetupMountain => {
                        let tmp = c.unwrap();
                        app.board_gui.board.board[tmp.x][tmp.y].piece = Some(
                            Piece::new(!app.teamcolor, Coord::new(tmp.x, tmp.y), PieceKind::Mountain)
                        );
                        app.playing_color = !app.playing_color;
                        app.mountain_count.1 = app.mountain_count.1 - 1;
                        if app.mountain_count.0 == 0 
                            && app.mountain_count.1 == 0 {
                            app.game_state = GameState::Playing;
                        }
                    },
                    GameState::Playing => {
                        let tmp = c.unwrap();
                        println!("({}, {}) | ({}, {})", c.unwrap().x, c.unwrap().y, c2.unwrap().x, c2.unwrap().y);
                        app.board_gui.board.board[tmp.x][tmp.y].piece.unwrap().move_to(
                            &mut app.board_gui.board,
                            c2.unwrap()
                        );
                        app.playing_color = !app.playing_color;
                        app.check_win();
                    },
                    GameState::EndGame => {
                        ()
                    }
                }
            },
            Err(_err) => (),
        };
    }


    
}