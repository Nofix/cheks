use std::net::TcpStream;

use crate::gui::gui::*;
use gamelogic::game::player::TeamColor;

pub fn start_game(stream: TcpStream, teamcolor: TeamColor) {
    gui(teamcolor, stream);
}