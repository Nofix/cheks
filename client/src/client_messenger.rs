use std::net::TcpStream;
use gamelogic::messenger::messenger;
use gamelogic::messenger::messenger::Message;
use gamelogic::messenger::messenger::MessageType;
use gamelogic::game::player::TeamColor;
use gamelogic::game::player::Coord;

use serde_json::{Result};

pub fn send_loby_code(stream: &mut TcpStream, loby_code: String) -> Result<()> {
    let m : Message<String> = Message::new(loby_code, MessageType::LobyCode);
    messenger::send_message(stream, m);
    Ok(())
}

pub fn recv_color(stream: &mut TcpStream) -> TeamColor {
    let c : Message<TeamColor> = messenger::recv_message(stream).unwrap();
    c.message
}

pub fn recv_validation(stream: &mut TcpStream) -> bool {
    let c : Message<bool> = messenger::recv_message(stream).unwrap();
    if c.message_type == MessageType::Ack {
        return c.message;
    }
    false
}

pub fn recv_piece_move(stream: &mut TcpStream) -> (Coord, Coord) {
    let c : Message<(Coord, Coord)> = messenger::recv_message(stream).unwrap();
    c.message
}

pub fn send_piece_move(stream: &mut TcpStream, from: Coord, to: Coord) -> bool {
    let m : Message<(Coord, Coord)> = Message::new((from, to), MessageType::Coord);
    match messenger::send_message(&mut stream.try_clone().unwrap(), m) {
        Ok(_m) => println!("Successfuly sent piece coord to server."),
        Err(e) => println!("{:?}", e)
    }
    return recv_validation(stream);
}

pub fn send_mountain_coord(stream: &mut TcpStream, coord: Coord) -> bool {
    let m : Message<Coord> = Message::new(coord, MessageType::Coord);
    match messenger::send_message(&mut stream.try_clone().unwrap(), m) {
        Ok(m) => println!("Successfuly sent mountain coord to server."),
        Err(e) => println!("{:?}", e)
    }
    return recv_validation(stream);
}

pub fn recv_mountain_coord(stream: &mut TcpStream) -> Coord {
    let c : Message<Coord> = messenger::recv_message(stream).unwrap();
    c.message
}