
extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;

#[macro_use]
extern crate conrod_core;

use gamelogic::custom_errors::*;
use gamelogic::game::player::TeamColor;

use std::net::{TcpStream};
use std::env;
use std::io::{Error, ErrorKind, Result};
use std::io::stdin;
use std::io::stderr;
use std::io::Write;
use std::str;

pub mod client_messenger;
pub mod gui {
    pub mod gui;
}
pub mod game;


static mut player_color: Option<TeamColor> = None;


fn ask_gamecode() -> std::result::Result<String, loby_code_error::ErrorCode> {
    let mut game_code = String::new();
    let len = stdin().read_line(&mut game_code).expect("Did not enter a correct string");
    // let mut reader = BufReader::new(stream);
    // let len: usize = reader.read_line(&mut game_code)?;
    match len {
        9 => len, // 8 chars + \n
        _ => return Err(loby_code_error::ErrorCode::WrongCode),
    };
    Ok(game_code)
}


fn client(address: &str, port: &str) -> std::io::Result<()> {
    
    
    let (color, mut stream) = loop {
        println!("Give me the code of the game you need to join");
        match ask_gamecode() {
            Ok(loby_code) => {
                let mut stream = TcpStream::connect(format!("{}:{}", address, port));
                match stream {
                    Ok(mut stream) => {
                        println!("Sending loby code");
                        let mut stream_cloned = stream.try_clone().unwrap();
                        client_messenger::send_loby_code(&mut stream, loby_code);
                        println!("waiting for response");
                        let color: TeamColor;
                        unsafe {
                            player_color = Some(client_messenger::recv_color(&mut stream));
                            color = player_color.unwrap();
                        }
                        break (color, stream_cloned);
                    },
                    Err(e) => {
                        stderr().write(format!("{:?}", e).as_bytes())?;
                    }
                };
            }
            Err(e) => {
                stderr().write(format!("{:?}", e).as_bytes())?;
            }
        };
    };
    
    println!("{}", color);
    game::start_game(stream, color);
    Ok(())
}

fn help(arg0: &str) {
    eprintln!("Usage : {} <ip_server> <port>", arg0);
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();

    match args.len() {
        2 => {
            // parse the command
            match args[1].as_str() {
                "--help" | "-h" | "h" | "help" => {
                    help(args[0].as_str());
                    return Ok(());
                },
                _ => {
                    // eprintln!("error: invalid command");
                    help(args[0].as_str());
                    return Err(Error::new(ErrorKind::InvalidInput, "Invalid arguments"));
                }
            }
        }
        3 => {
            client(args[1].as_str(), args[2].as_str())?;
        }
        _ => {
            // show a help message
            help(args[0].as_str());
            return Err(Error::new(ErrorKind::InvalidInput, "Invalid arguments"));
        }
    };

    Ok(())
}