use std::env;
use std::io::{Error, ErrorKind};

pub mod server;
// pub mod server {
//     pub mod server;
// }
// pub mod custom_errors {
//     pub mod game_error;
//     pub mod loby_code_error;
// }
// extern crate game_logic; 
// pub mod game_logic {
    // pub mod player;
// }


use crate::server::start_server;

fn help(arg0: &str) {
    eprintln!("Usage : {} <ip> <port>", arg0);
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();

    match args.len() {
        2 => {
            // parse the command
            match args[1].as_str() {
                "--help" | "-h" | "h" | "help" => {
                    help(args[0].as_str());
                    return Ok(());
                },
                _ => {
                    // eprintln!("error: invalid command");
                    help(args[0].as_str());
                    return Err(Error::new(ErrorKind::InvalidInput, "Invalid arguments"));
                }
            }
        }
        3 => {
            start_server(args[1].as_str(), args[2].as_str())?;
        }
        _ => {
            // show a help message
            help(args[0].as_str());
            return Err(Error::new(ErrorKind::InvalidInput, "Invalid arguments"));
        }
    };

    Ok(())
}