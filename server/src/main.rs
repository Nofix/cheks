#[macro_use] extern crate gamelogic;
use gamelogic::game::player::*;
use gamelogic::custom_errors::*;
use gamelogic::messenger::messenger;
use gamelogic::messenger::messenger::Message;
use std::io::Write;
use std::net::{TcpStream, TcpListener};
use std::thread;
use std::collections::{HashMap};
use std::sync::mpsc::sync_channel;
use std::sync::mpsc::SyncSender;
use std::env;
use std::io::{Error, ErrorKind};
use enum_iterator::IntoEnumIterator;

use gamelogic::game::player::*;
use gamelogic::game::piece_logic::*;

pub mod server_messenger;

// #[macro_use] extern crate gamelogic;

pub fn start_server(address: &str, port: &str) -> std::io::Result<()> {
    let (tx, rx) = sync_channel::<(TcpStream, String)>(4096); 
    let mut loby_list : HashMap<String, TcpStream> = HashMap::new(); // todo: Use a mutex
    thread::spawn(move || {
        loop{
            for (stream, code) in rx.recv() {
                let s = stream.try_clone().unwrap(); // TODO : HANDLE ERROR !
                print!("{} from {}", stream.peer_addr().unwrap(), code);
                match loby_list.get(&code) { 
                    None => {
                        println!("Code doesn't exist yet, creating a loby");
                        loby_list.insert(code, s);
                    }
                    Some(waiting_client) => {
                        let first_stream = waiting_client.try_clone().unwrap();
                        let second_stream = stream.try_clone().unwrap();
                        let mut second_stream_for_error = stream.try_clone().unwrap();
                        match Game::new(first_stream, second_stream) {
                            Ok(g) => {
                                loby_list.remove(&code);
                                thread::spawn(move || {
                                    return start_game(g);
                                 });
                            }
                            Err(e) => {
                                second_stream_for_error.write(format!("{:?}", e).as_bytes());
                            }
                        }
                    }
                }
            }
        }
    });

    let listener = TcpListener::bind(format!("{}:{}", address, port))?;

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        
        let remote_address = stream.peer_addr().unwrap();
        let tx = tx.clone();
        thread::spawn(move || {
            match game_matcher(stream, tx) {
                Ok(loby) => loby,
                Err(e) => {
                    eprint!("{:?}", e);
                    eprintln!(" from {}", remote_address);
                }
            };
        });
    }
    Ok(())
}


/**
 * To start a game, someone needs to instanciate a room and the other
 * player has to join it. To do that, the server asks for an 8 chars code that
 * will be used to identify the game. This function asks for the code and returns
 * it if it's valid, and returns an error otherwise.
 */
fn server_ask_gamecode(mut stream: TcpStream) -> std::result::Result<String, loby_code_error::ErrorCode> {
    println!("[Server] is waiting for a code");
    let loby_code: Message<String> = messenger::recv_message(&mut stream).unwrap();

    println!("got code");
    let len = loby_code.message.len();
    match len {
        9 => {// 8 chars + \n
            // match server_messenger::send_message(&mut stream, None) {
                // Ok(v) => return Ok(loby_code.message),
                // Err(e) => {
                    // println!("Err in sending Ok msg");
                    // return Err(loby_code_error::ErrorCode::WrongCode);
                // } 
            // }
            len
        }, 
        _ => {
            // server_messenger::send_message(&mut stream, Some(game_error::GameError::WrongCode));
            return Err(loby_code_error::ErrorCode::WrongCode);
        },
    };
    Ok(loby_code.message)
}

pub fn start_game(mut game: Game) -> std::result::Result<(), ()> {
    println!("Starting a game ! P1 : {} / P2 : {}", game.get_ip_player(TeamColor::White), game.get_ip_player(TeamColor::Black));
    server_messenger::send_color(&mut game.get_player_mut(TeamColor::White).fd, TeamColor::White);
    server_messenger::send_color(&mut game.get_player_mut(TeamColor::Black).fd, TeamColor::Black);
    // println!("{}", game);

    while game.game_state == GameState::SetupMountain {
        for team in TeamColor::into_enum_iter() {
            println!("Waiting for mountain from {:?}", team);
            loop {
                let mut s_playing = game.get_player_mut(team).get_stream();
                let mut s_waiting = game.get_player_mut(!team).get_stream();
                println!("Recv mountain coord from {:?}", team);
                let m_coord = server_messenger::recv_mountain_position(&mut s_playing).unwrap();
                if Board::is_in_team_bound(m_coord, team) && 
                    game.board.board[m_coord.x][m_coord.y].piece.is_none() {
                    println!("Send mountain validation to {:?}", team);
                    server_messenger::send_validation(&mut s_playing, team, true);
                    game.get_player_mut(team).mountain_count -= 1;
                    
                    server_messenger::send_mountain_coord(&mut s_waiting, team, m_coord);
                    game.board.board[m_coord.x][m_coord.y].piece = Some(
                        Piece::new(team, m_coord, PieceKind::Mountain)
                    );
                    break;
                }
            }
            if game.get_player_mut(team).mountain_count == 0 
                && game.get_player_mut(!team).mountain_count == 0 {
                    game.game_state = GameState::Playing;
            } 
        }
    }

    println!("[server] Starting the game now");
    while game.game_state == GameState::Playing {
        for team in TeamColor::into_enum_iter() {
            println!("Waiting for {:?} to move a piece", team);
            loop {
                let mut s_playing = game.get_player_mut(team).get_stream();
                let mut s_waiting = game.get_player_mut(!team).get_stream();
                println!("Recving move from {:?}", team);
                let (org_coord, dst_coord) = server_messenger::recv_piece_move(&mut s_playing).unwrap();
                println!("Received move from {:?}", team);
                if is_in_bounds!((0,0), (dst_coord.x, dst_coord.y)) {
                    // TODO METTRE UN CHECK SALE FDP
                    println!("Send move piece validation to {:?}", team);
                    server_messenger::send_validation(&mut s_playing, team, true);
                    server_messenger::send_piece_move(&mut s_waiting, team, org_coord, dst_coord);
                    break;
                }
            }
            // setup game win condition
        }
    }
    Ok(())
}

pub fn game_matcher(stream: TcpStream, tx: SyncSender<(TcpStream, String)>) -> std::result::Result<(), loby_code_error::ErrorCode> {
    println!("Client connected. Waiting for the code.");
    let cloned_steam = stream.try_clone().unwrap();
    let loby_code = loop {
        match server_ask_gamecode(stream) {
            Ok(loby_code) => {
                break loby_code;
            }
            Err(e) => return Err(e),
        };
    };
    
    tx.send((cloned_steam, loby_code)).unwrap();
    Ok(())
}


fn help(arg0: &str) {
    eprintln!("Usage : {} <ip> <port>", arg0);
}



fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();

    match args.len() {
        2 => {
            // parse the command
            match args[1].as_str() {
                "--help" | "-h" | "h" | "help" => {
                    help(args[0].as_str());
                    return Ok(());
                },
                _ => {
                    // eprintln!("error: invalid command");
                    help(args[0].as_str());
                    return Err(Error::new(ErrorKind::InvalidInput, "Invalid arguments"));
                }
            }
        }
        3 => {
            start_server(args[1].as_str(), args[2].as_str())?;
        }
        _ => {
            // show a help message
            help(args[0].as_str());
            return Err(Error::new(ErrorKind::InvalidInput, "Invalid arguments"));
        }
    };

    Ok(())
}