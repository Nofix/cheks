extern crate gamelogic;
use gamelogic::messenger::messenger::Message;
use gamelogic::messenger::messenger::MessageType;
use gamelogic::custom_errors::game_error::GameError;
use gamelogic::game::player::TeamColor;
use gamelogic::game::player::Coord;
use std::net::TcpStream;
use serde_json::{Result};
use serde::de::DeserializeOwned;
use serde::Deserialize;
use gamelogic::messenger::messenger;

#[derive(serde::Serialize, Deserialize, Debug)]
pub struct ServerMessenger {
    // The server returns true or false on receiving a message, else an error
    pub response: std::result::Result<(), GameError>, 
}

impl ServerMessenger {
    pub fn new(e: Option<GameError>) -> ServerMessenger {
        ServerMessenger { 
            response: match e {
                Some(e) => Err(e),
                _ => Ok(()),
            } 
        }
    }
}

pub fn recv_loby_code(stream: &mut TcpStream) -> String {
    let m: Message<String> = messenger::recv_message(stream).unwrap();
    m.message
}

pub fn send_color(stream: &mut TcpStream, color: TeamColor) -> Result<()> {
    messenger::send_message(stream, Message::new(color, MessageType::Color));
    Ok(())
}

pub fn recv_mountain_position(stream: &mut TcpStream) -> serde_json::Result<Coord> {
    let m: Message<Coord> = messenger::recv_message(stream).unwrap();
    Ok(m.message)
}

pub fn recv_piece_move(stream: &mut TcpStream) -> serde_json::Result<(Coord, Coord)> {
    let m: Message<(Coord, Coord)> = messenger::recv_message(stream).unwrap();
    Ok(m.message)
}

pub fn send_validation(stream: &mut TcpStream, color: TeamColor, val: bool) -> Result<()> {
    messenger::send_message(stream, Message::new(val, MessageType::Ack));
    Ok(())
}
pub fn send_mountain_coord(stream: &mut TcpStream, color: TeamColor, coord: Coord) -> Result<()> {
    messenger::send_message(stream, Message::new(coord, MessageType::Coord));
    Ok(())
}

pub fn send_piece_move(stream: &mut TcpStream, color: TeamColor, orig: Coord, dst: Coord) -> Result<()> {
    messenger::send_message(stream, Message::new((orig, dst), MessageType::PieceMovement));
    Ok(())
}

// // This function sends a message to a client to validate his move or not.
// pub fn send_message(stream: &mut TcpStream, e: Option<GameError>) -> Result<()> {
//     let server_messenger = ServerMessenger::new(e);
//     serde_json::to_writer(stream, &server_messenger)?;
//     Ok(())
// }

// pub fn receive_message<T>(stream: &mut TcpStream) -> std::result::Result<Message<T>, Box<dyn std::error::Error>> 
// where T: DeserializeOwned
// {
//     let mut response = serde_json::Deserializer::from_reader(stream);
//     Ok(Message::deserialize(&mut response)?)
// }