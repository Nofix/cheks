use std::fmt;

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub enum GameError {
    WrongCode,
    TooManyPlayers,
}

impl fmt::Display for GameError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            GameError::TooManyPlayers => write!(f, "Game is full or already started."),
            GameError::WrongCode => write!(f, "Input code is invalid."),
        }
    }
}
