use std::fmt;

#[derive(Debug)]
pub enum ErrorCode {
    IoError(std::io::Error),
    WrongCode,
}

impl From<std::io::Error> for ErrorCode {
    fn from(cause: std::io::Error) -> Self {
        ErrorCode::IoError(cause)
    }
}

impl fmt::Display for ErrorCode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ErrorCode::IoError(ref cause) => write!(f, "I/O Error: {}", cause),
            ErrorCode::WrongCode => write!(f, "The user given code is not valid."),
        }
    }
}
