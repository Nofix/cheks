use std::net::TcpStream;
use rand::Rng;
use std::ops::Not;
use serde::{Deserialize, Serialize};
use std::fmt;
use crate::custom_errors::game_error::*;
use crate::game::piece_logic::Piece;
use crate::game::piece_logic::PieceKind;
use std::ops::{Add};
use enum_iterator::IntoEnumIterator;

#[derive(PartialEq)]
pub enum GameState {
    SetupMountain,
    Playing,
    EndGame
}
impl fmt::Display for GameState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            GameState::SetupMountain  => f.write_str("Setup your montains")?,
            GameState::Playing        => f.write_str("Playing")?,
            GameState::EndGame        => f.write_str("End of the game!")?,
        };
        Ok(())
    }
}


#[derive(Copy, Clone,  Eq, Hash, PartialEq, Debug, Serialize, Deserialize)]
pub struct Coord {pub x: usize, pub y: usize}
impl Coord {
    pub fn new(x: usize, y: usize) -> Coord{
        Coord{x,y}
    }
}


#[derive(Copy, Clone, PartialEq, Serialize, Deserialize, Debug)]
pub enum CaseColor {
    Black,
    White,
    Red,
    // Selected,
    OutOfBound
}

#[derive(Copy, Clone, IntoEnumIterator, PartialEq, Serialize, Deserialize, Debug)]
pub enum TeamColor {
    White,
    Black
}

impl Not for TeamColor {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            TeamColor::White => TeamColor::Black,
            TeamColor::Black => TeamColor::White,
        }
    }
}


impl fmt::Display for TeamColor {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            TeamColor::White  => fmt.write_str("White")?,
            TeamColor::Black  => fmt.write_str("Black")?,
        }
        Ok(())
    }
}
impl Default for TeamColor {
    fn default() -> TeamColor {
        TeamColor::White
    }
}

impl fmt::Display for CaseColor {
    fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            CaseColor::White  => print!("\x1b[47;30m "),
            CaseColor::Black  => print!("\x1b[40;37m "),
            CaseColor::Red    => print!("\x1b[31;42m "),
            // CaseColor::Selected => print!("\x1b[34;40m "),
            CaseColor::OutOfBound => print!("\x1b[0m "),
        }
        Ok(())
    }
}
impl Default for CaseColor {
    fn default() -> CaseColor {
        CaseColor::OutOfBound
    }
}


pub struct Player{
    pub fd : TcpStream,
    pub color: TeamColor,
    pub mountain_count: i8
    // status: Status,
}

impl Player {
    pub fn new(filedescriptor: TcpStream, color: TeamColor) -> Player {
        Player{
            fd: filedescriptor, 
            color: color, 
            mountain_count: 6
        }
    }
}

impl Player {
    pub fn get_stream(&mut self) -> TcpStream {
        self.fd.try_clone().unwrap()
    }
}

#[derive(Clone, Copy)]
pub struct Case {
    pub color: CaseColor,
    pub piece: Option<Piece>,
    pub selected: bool
}

impl Case {
    pub fn new(color: CaseColor, piece: Option<Piece>) -> Case {
        Case{ 
            color, 
            piece,
            selected: false
        }
    }
    pub fn is_empty(&self) -> bool {
        return self.piece.is_none();
    }
    pub fn get_color(&self) -> CaseColor {
        self.color
    }
    pub fn set_color(mut self, color: CaseColor) {
        self.color = color;
    }
    pub fn unselect(&mut self){
        self.selected = false;
    }
}

impl Default for Case {
    fn default() -> Case {
        Case {color: CaseColor::default(), piece: None, selected: false}
    }
}


impl fmt::Display for Case {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.piece.is_none() {
            fmt.write_str(format!("{}", self.color).as_str())?;
        } else {
            fmt.write_str(format!("{}", self.piece.unwrap()).as_str())?;
        }
        Ok(())
    }
}

#[macro_export]
macro_rules! is_in_bounds {
    ( $first_coord:expr, $second_coord:expr ) => {
        {
            (
                ($first_coord.0 as i64) + ($second_coord.0 as i64) < Board::W as i64 &&
                ($first_coord.0 as i64) + ($second_coord.0 as i64) >= 0              &&
                ($first_coord.1 as i64) + ($second_coord.1 as i64) < Board::H as i64 - 1 &&
                ($first_coord.1 as i64) + ($second_coord.1 as i64) >= 1
            ) 
            ||
            (
                (($first_coord.0 as i64) + ($second_coord.0 as i64) == 4 && (($first_coord.1 as i64) + ($second_coord.1 as i64) ==  0)) ||
                (($first_coord.0 as i64) + ($second_coord.0 as i64) == 4 && (($first_coord.1 as i64) + ($second_coord.1 as i64) == 13)) 
            )
        }
    };
}
pub struct Board {
    pub board: [[Case;Board::H];Board::W],
}
impl Board {
    pub const H: usize = 14;
    pub const W: usize = 9;
    pub const default_board: &'static str = "9/1BR3BR1/1GGG1GGG1/9/1T5T1/9/9/9/9/1T5T1/9/1GGG1GGG1/1RB3RB/9";
    pub fn new() -> Board {
        Board::default()
    }
    // Reset all user piece selections
    pub fn reset_selections(&mut self) {
        for case in self.board.iter_mut().flat_map(|r| r.iter_mut()) {
            case.unselect();
        }
    }
    // Checks if given coordonates are in the team side or not.
    pub fn is_in_team_bound(coord: Coord, team: TeamColor) -> bool {
        if !is_in_bounds!((0,0), (coord.x,coord.y)) {
            return false;
        }
        return match team {
            TeamColor::White => coord.y <  (Board::H / 2),
            TeamColor::Black => coord.y >= (Board::H / 2),
        };
    }
}
impl Default for Board {
    fn default() -> Board {
        // board is 12*9 plus the winning Case, e.g:
        //         ██
        //   ██  ██  ██  ██  
        // ██  ██  ██  ██  ██
        //   ██  ██  ██  ██  
        // ██  ██  ██  ██  ██
        //   ██  ██  ██  ██  
        // ██  ██  ██  ██  ██
        //   ██  ██  ██  ██  
        // ██  ██  ██  ██  ██
        //   ██  ██  ██  ██  
        // ██  ██  ██  ██  ██
        //   ██  ██  ██  ██  
        // ██  ██  ██  ██  ██
        //         ██
        //
        let mut board = [[Case::default();Board::H]; Board::W];
        let mut cnt = 0;
        // Forsyth-Edwards Notation
        let default_board = Board::default_board.as_bytes();
        for H in 0..Board::H {
            for W in 0..Board::W {
                if H == 0 || H == Board::H - 1 {
                    if W == 4 {
                        board[W][H] = Case::new(CaseColor::Red, None);
                    } else {
                        board[W][H] = Case::new(CaseColor::OutOfBound, None);
                    }
                } else {
                    board[W][H] = match cnt % 2 {
                        0 => Case::new(CaseColor::White, None),
                        _ => Case::new(CaseColor::Black, None),
                    };
                }
                cnt += 1;
            }
        }

        // I know this looks really ugly, but as long as I don't need the be able
        // to set a full board with a given string in a Forsyth-Edwards fashion,
        // this is the most readable, and fast method of filling the default board.
        // (fast to write, and fast to execute at runtime)
        
        //
        // White
        //

        // Bishops
        board[1][1].piece = Some(Piece::new(TeamColor::White, Coord::new(1, 1), PieceKind::Bishop));
        board[6][1].piece = Some(Piece::new(TeamColor::White, Coord::new(6, 1), PieceKind::Bishop));

        // Rooks
        board[2][1].piece = Some(Piece::new(TeamColor::White, Coord::new(2, 1), PieceKind::Rook));
        board[7][1].piece = Some(Piece::new(TeamColor::White, Coord::new(7, 1), PieceKind::Rook));

        // Guards
        board[1][2].piece = Some(Piece::new(TeamColor::White, Coord::new(1, 2), PieceKind::Guard));
        board[2][2].piece = Some(Piece::new(TeamColor::White, Coord::new(2, 2), PieceKind::Guard));
        board[3][2].piece = Some(Piece::new(TeamColor::White, Coord::new(3, 2), PieceKind::Guard));
        board[5][2].piece = Some(Piece::new(TeamColor::White, Coord::new(5, 2), PieceKind::Guard));
        board[6][2].piece = Some(Piece::new(TeamColor::White, Coord::new(6, 2), PieceKind::Guard));
        board[7][2].piece = Some(Piece::new(TeamColor::White, Coord::new(7, 2), PieceKind::Guard));

        // Towers
        board[1][4].piece = Some(Piece::new(TeamColor::White, Coord::new(1, 4), PieceKind::Tower));
        board[7][4].piece = Some(Piece::new(TeamColor::White, Coord::new(7, 4), PieceKind::Tower));

        //
        // Black
        //

        board[1][12].piece = Some(Piece::new(TeamColor::Black, Coord::new(1, 12), PieceKind::Bishop));
        board[6][12].piece = Some(Piece::new(TeamColor::Black, Coord::new(6, 12), PieceKind::Bishop));

        // Rooks
        board[2][12].piece = Some(Piece::new(TeamColor::Black, Coord::new(2, 12), PieceKind::Rook));
        board[7][12].piece = Some(Piece::new(TeamColor::Black, Coord::new(7, 12), PieceKind::Rook));

        // Guards
        board[1][11].piece = Some(Piece::new(TeamColor::Black, Coord::new(1, 11), PieceKind::Guard));
        board[2][11].piece = Some(Piece::new(TeamColor::Black, Coord::new(2, 11), PieceKind::Guard));
        board[3][11].piece = Some(Piece::new(TeamColor::Black, Coord::new(3, 11), PieceKind::Guard));
        board[5][11].piece = Some(Piece::new(TeamColor::Black, Coord::new(5, 11), PieceKind::Guard));
        board[6][11].piece = Some(Piece::new(TeamColor::Black, Coord::new(6, 11), PieceKind::Guard));
        board[7][11].piece = Some(Piece::new(TeamColor::Black, Coord::new(7, 11), PieceKind::Guard));

        // Towers
        board[1][9].piece = Some(Piece::new(TeamColor::Black, Coord::new(1, 9), PieceKind::Tower));
        board[7][9].piece = Some(Piece::new(TeamColor::Black, Coord::new(7, 9), PieceKind::Tower));

        Board{board}
    }

}
impl fmt::Display for Board {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, row) in self.board.iter().enumerate() {
            for (j, col) in row.iter().enumerate() {
                fmt.write_str(format!("{}", col).as_str())?;
            }
            fmt.write_str(format!("{}\n", CaseColor::OutOfBound).as_str())?;
        }
        Ok(())
    }
}

impl Board {
    fn move_piece(&mut self, from: Coord, to: Coord) -> bool {
        if is_in_bounds!((0,0), (from.x, from.y)) && 
            is_in_bounds!((0,0), (to.x, to.y)) { 
            if self.board[from.x][from.y].piece.is_some() {
                self.board[from.x][from.y].piece = None;
                if self.board[from.x][from.y].piece.unwrap().move_to(self, to) {
                    return true;
                }
            }   
        }
        false
    }

    pub fn is_mountain_at(&self, coord: Coord) -> bool {
        if self.board[coord.x][coord.y].piece.is_some() {
            return self.board[coord.x][coord.y].piece.unwrap().kind == PieceKind::Mountain;
        }
        false
    }

    // One rule of the game is that you cannot put more than 3 
    // mountains on the same line. 
    // The player also cannot put montains too close to his pieces.
    // This function assumes that caller's checks that the montain
    // coord is in the player's side. 
    pub fn can_set_mountain(&mut self, coord: Coord) -> bool {
        if self.is_mountain_at(coord) {return false};
        if coord.y < 3 || coord.y > 10 {return false};
        let y = coord.y;
        let mut cnt = 0;
        for i in 0..Board::W {
            if self.is_mountain_at(Coord{x:i,y}) {
                cnt += 1;
            }
        }
        return cnt < 3;
    }
}

pub struct Game {
    players: [Option<Player>; 2],
    pub nb_of_players: u8,
    playing_color: TeamColor,
    pub board: Board,
    pub game_state: GameState
}

impl Game {
    pub fn get_board(&self) -> [[Case;Board::H];Board::W] {
        self.board.board
    }

    pub fn new(stream_one: TcpStream, stream_two: TcpStream) -> std::result::Result<Game, GameError> {
        
        let mut game = Game {
            players : [None, None],
            nb_of_players: 0,
            playing_color: TeamColor::White,
            board: Board::default(),
            game_state: GameState::SetupMountain
        };
        game.players[0] = Some(game.new_player(stream_one)?);
        game.players[1] = Some(game.new_player(stream_two)?);
        return Ok(game);
    }



    pub fn get_player(&mut self, color_to_find: TeamColor) -> &Player {
        if self.players[0].as_ref().unwrap().color == color_to_find {
            return self.players[0].as_ref().unwrap();
        } 
        self.players[1].as_ref().unwrap()
    }

    pub fn get_player_mut(&mut self, color_to_find: TeamColor) -> &mut Player {
        if self.players[0].as_ref().unwrap().color == color_to_find {
            return self.players[0].as_mut().unwrap();
        } 
        self.players[1].as_mut().unwrap()
    }

    // pub fn send_data(&mut self, data: &[u8], color: Color) {
    //     self.get_player_mut(color).fd.write(data);
    // }

    pub fn get_ip_player(&mut self, color: TeamColor) -> std::net::SocketAddr {
        return self.get_player(color).fd.peer_addr().unwrap();
    }

    /**
     * Creates a new player in the game. There can only be 2 players.
     * The color of the first player is selected randomly.
     * */
    fn new_player(&mut self, stream : TcpStream) -> std::result::Result<Player, GameError> {
        let player = match self.nb_of_players {
            0 => {
                let mut rng = rand::thread_rng();
                let player = Player::new(stream, 
                            if rng.gen::<u8>() == 0 { TeamColor::White } else { TeamColor::Black },
                );
                player
            },
            1 => {
                Player::new(stream, !self.players[0].as_ref().unwrap().color)
            },
            _ => return Err(GameError::TooManyPlayers),
        };
        self.nb_of_players += 1;
        return Ok(player);
    }


}

impl fmt::Display for Game {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, row) in self.board.board.iter().enumerate() {
            for (j, col) in row.iter().enumerate() {
                fmt.write_str(format!("{}", col).as_str())?;
            }
            fmt.write_str(format!("{}\n", CaseColor::OutOfBound).as_str())?;
        }
        Ok(())
    }
}
