use serde::{Deserialize, Serialize};
use std::fmt;
use crate::game::player::TeamColor;
use crate::game::player::Coord;
use crate::game::player::Game;
use crate::game::player::Board;
use crate::game::pieces::bishop;
use crate::game::pieces::mountain;
use crate::game::pieces::guard;
use crate::game::pieces::rook;
use crate::game::pieces::tower;

// old macro
//
// #[macro_export]
// macro_rules! add_move_if_case_empty_and_break_on_capture{
//     ($self:expr, $game:expr, $x:expr, $y:expr, $p:expr) => {{
//         let c = Coord::new(
//             ($self.coord.x as i64 + $x) as usize, 
//             ($self.coord.y as i64 + $y) as usize
//         );
//         // If the case is empty, the move is valid
//         if $game.get_board()[c.x][c.y].is_empty() {
//             $p.push(c);
//         } else {
//             // else, if the case holds an enemy piece, the move is valid
//             // and we end the loop
//             if $game.get_board()[c.x][c.y].piece.unwrap().color != $self.color {
//                 $p.push(c);
//                 break;
//             }
//             // we end the loop if the piece isn't an enemy piece
//             break;
//         }
//     }};
// }



#[derive(Copy, Clone, PartialEq, Serialize, Deserialize, Debug)]
pub enum PieceKind {
    Bishop   = 0,
    Tower    = 1,
    Rook     = 2,
    Guard    = 3,
    Mountain = 4,
}

impl fmt::Display for PieceKind {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            PieceKind::Tower    => fmt.write_str(tower   ::REPRESENTATION)?,
            PieceKind::Rook     => fmt.write_str(rook    ::REPRESENTATION)?,
            PieceKind::Guard    => fmt.write_str(guard   ::REPRESENTATION)?,
            PieceKind::Bishop   => fmt.write_str(bishop  ::REPRESENTATION)?,
            PieceKind::Mountain => fmt.write_str(mountain::REPRESENTATION)?,
        };
        Ok(())
    }
}

#[derive(Copy, Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct Piece {
    pub color: TeamColor,
    pub coord: Coord,
    pub kind : PieceKind,
    pub charged: bool, // mechanic only useful for Guard, see guard.rs
    representation : &'static str,
}

impl Piece {

    pub fn new(color: TeamColor, coord: Coord, kind: PieceKind) -> Piece {
        Piece{color,coord,kind, charged:false,
            representation: match kind {
                PieceKind::Tower            => tower::REPRESENTATION,
                PieceKind::Rook             => rook::REPRESENTATION,
                PieceKind::Guard            => guard::REPRESENTATION,
                PieceKind::Bishop           => bishop::REPRESENTATION,
                PieceKind::Mountain         => mountain::REPRESENTATION,
            }
        }
    }
    

    pub fn can_move(&self, to: Coord, board: &Board) -> bool{
        match self.kind {
            PieceKind::Tower    => tower   ::can_move(self, to, board),
            PieceKind::Rook     => rook    ::can_move(self, to, board),
            PieceKind::Bishop   => bishop  ::can_move(self, to, board),
            PieceKind::Mountain => mountain::can_move(self, to, board),
            PieceKind::Guard    => guard   ::can_move(self, to, board),
        }
    }
    pub fn get_possible_moves(&self, board: &Board) -> Vec<Coord>{
        match self.kind {
            PieceKind::Tower    => tower   ::get_possible_moves(self, board),
            PieceKind::Rook     => rook    ::get_possible_moves(self, board),
            PieceKind::Bishop   => bishop  ::get_possible_moves(self, board),
            PieceKind::Mountain => mountain::get_possible_moves(self, board),
            PieceKind::Guard    => guard   ::get_possible_moves(self, board),
        }
    }
    pub fn generate_possible_moves(&self, board: &Board, moves:&[&'static[(i64,i64)]]) -> Vec<Coord> {
        let mut p : Vec<Coord> = Vec::new();
        for piece_move in moves.iter(){ // for each direction
            // as long as the move is in bounds
            
            for (x,y) in piece_move.iter().take_while(|xy|
                is_in_bounds!((self.coord.x, self.coord.y), xy) 
                // those conditions has been moved to the macro
                // || ((self.coord.x as i64) + (xy.0 as i64) == 4 && ((self.coord.y as i64) + (xy.1 as i64) ==  0)) 
                // || ((self.coord.x as i64) + (xy.0 as i64) == 4 && ((self.coord.y as i64) + (xy.1 as i64) == 13)) 
            ) {
                // add_move_if_case_empty_and_break_on_capture!(self, game, x, y, p);
                let c = Coord::new(
                    (self.coord.x as i64 + x) as usize, 
                    (self.coord.y as i64 + y) as usize
                );
                // println!("Checking move {} {} from {} {}", x, y, self.coord.x, self.coord.y);
                // If the case is empty, the move is valid
                if board.board[c.x][c.y].is_empty() {
                    p.push(c);
                } else {
                    // else, if the case holds an enemy piece, the move is valid
                    // and we end the loop
                    if board.board[c.x][c.y].piece.unwrap().color != self.color 
                        && board.board[c.x][c.y].piece.unwrap().kind != PieceKind::Mountain {
                        p.push(c);
                        break;
                    }
                    // we end the loop if the piece isn't an enemy piece
                    break;
                }
            }
        }  
        p
    }
    // returns false if move isn't possible, true instead
    pub fn move_to(&mut self, board: &mut Board, coord: Coord) -> bool {
        if self.can_move(coord, board) {
            if self.kind == PieceKind::Guard {
                self.charged = match self.color {
                    TeamColor::Black => {
                        if (coord.x - self.coord.x <= 1 && self.coord.y - coord.y <= 1)
                            || (self.coord.x - coord.x <= 1 && self.coord.y - coord.y <= 1) {
                            true
                        } else {
                            false
                        }
                    },
                    TeamColor::White => {
                        if (self.coord.x - coord.x <= 1 && coord.y - self.coord.y <= 1)
                        || (coord.x - self.coord.x <= 1 && coord.y - self.coord.y <= 1) {
                            true
                        } else {
                            false
                        }
                    }
                }
                
            }
            let (x,y) = (self.coord.x, self.coord.y);
            self.set_coord(coord);
            board.board[coord.x][coord.y].piece = Some(self.clone());
            board.board[x][y].piece = None;
            return true;
        }
        false
    }
    pub fn set_coord(&mut self, coord: Coord) {
        self.coord = coord;
    }
}
impl fmt::Display for Piece {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.color == TeamColor::Black {
            fmt.write_str(self.representation.to_ascii_lowercase().as_str())?;
        } else {
            fmt.write_str(self.representation)?;
        }
        
        Ok(())
    }
}

