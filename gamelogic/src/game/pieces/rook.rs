use crate::game::player::Board;
use crate::game::player::Coord;
use crate::game::piece_logic::Piece;

pub const REPRESENTATION : &'static str = "R";

const MOVES: [&[(i64, i64)]; 4] = [
        &[(0,1), (0,2), (0,3), (0,4), (0,5),(0,6),(0,7),(0,8),(0,9),(0,10),(0,11),(0,12),(0,13),(0,14)],                 // to bottom right
        &[(0,-1), (0,-2), (0,-3), (0,-4), (0,-5),(0,-6),(0,-7),(0,-8),(0,-9),(0,-10),(0,-11),(0,-12),(0,-13),(0,-14)],                 // to bottom right
        &[(-1,0), (-2,0), (-3,0), (-4,0), (-5,0),(-6,0),(-7,0),(-8,0),(-9,0),(-10,0),(-11,0),(-12,0),(-13,0),(-14,0)],         // to upper right
        &[(1,0), (2,0), (3,0), (4,0), (5,0),(6,0),(7,0),(8,0),(9,0),(10,0),(11,0),(12,0),(13,0),(14,0)],         // to upper right
];

pub fn can_move(piece: &Piece, to:Coord, board: &Board) -> bool {
    get_possible_moves(piece, board).contains(&to)
}

pub fn get_possible_moves(piece: &Piece, board: &Board) -> Vec<Coord>{
    piece.generate_possible_moves(board, &MOVES)
}
