use crate::game::player::Board;
use crate::game::player::Coord;
use crate::game::piece_logic::Piece;

pub const REPRESENTATION : &'static str = "T";

pub fn can_move(piece: &Piece, to:Coord, board: &Board) -> bool {
    get_possible_moves(piece, board).contains(&to)
}

pub fn get_possible_moves(_piece: &Piece, _board: &Board) -> Vec<Coord>{
    vec![]
}
