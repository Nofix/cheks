use crate::game::player::Board;
use crate::game::player::Coord;
use crate::game::piece_logic::Piece;

pub const REPRESENTATION : &'static str = "B";

// Possible movements on the board
const MOVES:[&[(i64, i64)]; 4] = [
    &[(1,1), (2,2), (3,3), (4,4), (5,5),(6,6),(7,7),(8,8)],                 // to bottom right
    &[(1,-1), (2,-2), (3,-3), (4,-4), (5,-5),(6,-6),(7,-7),(8,-8)],         // to upper right
    &[(-1,1), (-2,2), (-3,3), (-4,4), (-5,5),(-6,6),(-7,7),(-8,8)],         // to bottom left
    &[(-1,-1), (-2,-2), (-3,-3), (-4,-4), (-5,-5),(-6,-6),(-7,-7),(-8,-8)], // to upper left
];

pub fn can_move(piece: &Piece, to:Coord, board: &Board) -> bool {
    get_possible_moves(piece, board).contains(&to)
}

pub fn get_possible_moves(piece: &Piece, board: &Board) -> Vec<Coord>{
    piece.generate_possible_moves(board, &MOVES)
}
