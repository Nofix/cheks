use crate::game::player::Board;
use crate::game::player::Coord;
use crate::game::piece_logic::Piece;

pub const REPRESENTATION : &'static str = "M";

pub fn can_move(_piece: &Piece, _to:Coord, _board: &Board) -> bool {
    false
}
pub fn get_possible_moves(_piece: &Piece, _board: &Board) -> Vec<Coord>{
    vec![]
}
