use crate::game::player::TeamColor;
use crate::game::player::Board;
use crate::game::player::Coord;
use crate::game::piece_logic::Piece;
use crate::game::piece_logic::PieceKind;

use std::collections::HashSet;

pub const REPRESENTATION : &'static str = "G";

// At this game, Guards is a piece that can two 
// case in any direction, except backward 
// (as in chess, pawns cannot move backward).
// You can also choose to move your piece from only
// once case. In this case, the Guard is said "charged"
// and this exact same Guard will be able to move 
// from 3 cases on its next turn only.
// Here are its movements on a board:
// ██  ██  ██  ██  ██
//   ██  ██XX██  ██  
// ██  ██XXXXXX██  ██
//   ██XXXXGUXXXX██  
// ██  ██  ██  ██  ██
//
// Once charged:
//
// ██  ██  XX  ██  ██
//   ██  XXXXXX  ██  
// ██  XXXXXXXXXX  ██
//   XXXXXXGUXXXXXX  
// ██  ██  ██  ██  ██


const WHITE_MOVES: [&[(i64,i64)];7] = [
    &[( 1, 0), ( 2, 0)], // right
    &[(-1, 0), (-2, 0)], // left
    &[(-1, 0), (-1, 1)], // left downward
    &[( 1, 0), ( 1, 1)], // right downward
    &[( 0, 1), (-1, 1)], // downward left
    &[( 0, 1), ( 1, 1)], // downward right
    &[( 0, 1), ( 0, 2)], // downward
];

const WHITE_MOVES_CHARGED: [&[(i64,i64)];13] = [
    &[(-1, 0), (-2, 0), (-3, 0)], // <- <- <-
    &[(-1, 0), (-2, 0), (-2, 1)], // <- <- |
    &[(-1, 0), (-1, 1), (-2, 1)], // <- |  <-
    &[( 0, 1), (-1, 1), (-2, 1)], // |  <- <-
    &[(-1, 0), (-1, 1), (-1, 2)], // <- |  |
    &[( 0, 1), ( 0, 2), (-1, 2)], // |  |  <-

    &[( 0, 1), ( 0, 2), ( 0, 3)], // |  |  |

    &[( 1, 0), ( 2, 0), ( 3, 0)], // -> -> ->
    &[( 1, 0), ( 2, 0), ( 2, 1)], // -> -> |
    &[( 1, 0), ( 1, 1), ( 2, 1)], // -> |  ->
    &[( 0, 1), ( 1, 1), ( 2, 1)], // |  -> ->
    &[( 1, 0), ( 1, 1), ( 1, 2)], // -> |  |
    &[( 0, 1), ( 0, 2), ( 1, 2)], // |  |  ->
];

const BLACK_MOVES_CHARGED: [&[(i64,i64)];13] = [
    &[(-1, 0), (-2, 0), (-3, 0)], // <- <- <-
    &[(-1, 0), (-2, 0), (-2,-1)], // <- <- |
    &[(-1, 0), (-1,-1), (-2,-1)], // <- |  <-
    &[( 0,-1), (-1,-1), (-2,-1)], // |  <- <-
    &[(-1, 0), (-1,-1), (-1,-2)], // <- |  |
    &[( 0,-1), ( 0,-2), (-1,-2)], // |  |  <-

    &[( 0,-1), ( 0,-2), ( 0,-3)], // |  |  |

    &[( 1, 0), ( 2, 0), ( 3, 0)], // -> -> ->
    &[( 1, 0), ( 2, 0), ( 2,-1)], // -> -> |
    &[( 1, 0), ( 1,-1), ( 2,-1)], // -> |  ->
    &[( 0,-1), ( 1,-1), ( 2,-1)], // |  -> ->
    &[( 1, 0), ( 1,-1), ( 1,-2)], // -> |  |
    &[( 0,-1), ( 0,-2), ( 1,-2)], // |  |  ->
];

const BLACK_MOVES: [&[(i64,i64)];7] = [
    &[( 1, 0), ( 2, 0)], // right
    &[(-1, 0), (-2, 0)], // left
    &[(-1, 0), (-1,-1)], // left upper
    &[( 1, 0), ( 1,-1)], // right upper
    &[( 0,-1), (-1,-1)], // upper left
    &[( 0,-1), ( 1,-1)], // upper right
    &[( 0,-1), ( 0,-2)], // upper 
    // &[(1,-1), (2,0), (-1,-1), (-2,0), (0,-2)],
];



pub fn can_move(piece: &Piece, to:Coord, board: &Board) -> bool {
    get_possible_moves(piece, board).contains(&to)
}

pub fn get_possible_moves(piece: &Piece, board: &Board) -> Vec<Coord>{
    let mut possible_moves: Vec<Coord> = vec![];
    if let PieceKind::Guard = piece.kind {
        if piece.charged {
            let moves = match piece.color {
                TeamColor::White => WHITE_MOVES_CHARGED,
                TeamColor::Black => BLACK_MOVES_CHARGED,
                _ => return vec![],
            };
            possible_moves = piece.generate_possible_moves(board, &moves);
        } else {
            let moves = match piece.color {
                TeamColor::White => WHITE_MOVES,
                TeamColor::Black => BLACK_MOVES,
                _ => return vec![],
            };           
            possible_moves = piece.generate_possible_moves(board, &moves);
        }
    }
    // Removing duplicates
    let set: HashSet<Coord> = possible_moves.drain(..).collect();
    possible_moves.extend(set.into_iter());
    possible_moves
}