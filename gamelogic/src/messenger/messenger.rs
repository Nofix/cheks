use std::net::TcpStream;
use serde::{Deserialize, Serialize};
use serde::de::*;


/**
 * 
 * Le client doit recevoir des Ack, sa couleur, des coups joués par l'adversaire.
 * 
 */

// pub enum MessageType {
//     LobyCode,   // the message contains the loby code
//     Ack,        // mostly used by the server to acknoledge an information or deny something
//     Undef,
// }


// Server purpose is purely acknowledging client's informations and 
// answering if ever they are accepted or not.
//
// Should be going for custom error codes tho, e.g
// pub enum response {
//    Accepted,
//    Denied(CustomErr)
// }
//


#[derive(Serialize, PartialEq, Deserialize, Debug)]
pub enum MessageType {
    LobyCode,       // the message contains the loby code
    Coord,          // Coord
    PieceMovement,  // Coord
    Color,          // message contains attribued player color
    Ack,
    Undef,      // 
    // MovePiece(Piece),
}



#[derive(Serialize, Deserialize, Debug)]
pub struct Message<T> {
    // color: Option<Color>, // None for server, color for player
    pub message: T,
    pub message_type: MessageType
}

impl<T> Message<T> {
    pub fn new(message: T, message_type: MessageType) -> Message<T> {
        Message{message, message_type}
    }
}

pub fn send_message<T>(stream: &mut TcpStream, message: Message<T>) -> std::result::Result<(), Box<dyn std::error::Error>> 
where T: Serialize
{
    Ok(serde_json::to_writer(stream.try_clone().unwrap(), &message).unwrap())
}

pub fn recv_message<T>(stream: &mut TcpStream) -> std::result::Result<Message<T>, Box<dyn std::error::Error>> 
where T: DeserializeOwned
{
    let mut response = serde_json::Deserializer::from_reader(stream);
    Ok(Message::deserialize(&mut response).unwrap())
}