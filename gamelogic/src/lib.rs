
pub mod custom_errors {
    pub mod game_error;
    pub mod loby_code_error;
}

pub mod game {
    #[macro_use] pub mod player;
    pub mod piece_logic;
    pub mod pieces {
        pub mod bishop;
        pub mod guard;
        pub mod mountain;
        pub mod rook;
        pub mod tower;
    }
}

pub mod messenger {
    pub mod messenger;
}

