This is a chess-like network board game project I made in order to learn some rust.
This includes (yet) : 
- Threading
- mpsc (Multi-producer, single-consumer FIFO queue communication primitives.)
- Concurency
- Networking
- Custom error handling
- Project organisations (directories, use, crates, workspaces...)
- A client GUI (quite a mess but working)

The project has minimal crates dependencies because I wanted to learn as much as I can, without relying on magic crates doing the job for me.

**Working on rust version 1.47.0 due to outdated library used in piston2d crate (used for client GUI), see https://github.com/amethyst/amethyst/issues/2524**

TODO : 
- Huge refactor
- Write the clean rules somewhere so that people understand how this game is played
